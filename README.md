# Remote Backend Exercise

## Project Setup

Required versions of Elixir and Erlang are defined in the `.tool-versions` file.

If you use `asdf`, run `asdf install` to install any missing versions. These versions will be used locally.

Run `mix deps.get` to install all dependencies.

To set up the database, run `mix ecto.setup`. This step runs the initial seed of `1000000` users. It runs in batches, and while it does take a while, it shouldn't be too long.

## Running the project

To run the project, use `mix phx.server`, or `iex -S mix phx.server` to have console access.

To try out the project, use `curl`, a tool like Postman/Insomnia, or simply open [`localhost:4000`](http://localhost:4000) on your browser, you should see the expected JSON response.

## Testing and linting

This project has several tests, run them with `mix test`.

I've also set up credo and dialyzer. Run `mix credo` and `mix dialyzer` respectively to do verifications.

Run `mix format` if necessary after any changes.

## Documentation

Most of the project is thoroughly documented. Run `mix docs` to generate documentation files in `doc/`. Start by opening `doc/index.html` in your browser.

## Project Overview

You can find most of the core code in `lib/remote_backend_exercise`:

- **schemas/user.ex** - The `schemas` directory would be used for any Ecto schemas to be declared. User points validations are done here through Changeset.
- **users.ex** - Model manipulation functions. Find/add/update, etc. can be found here, as well as unique functionality such as `find_all_with_points_higher_than/1`
- **points_manager.ex** - The main functionality lies here, this GenServer makes use of the `Users` module for a lot of its needs.
- **application.ex** - I start the GenServer here, in the main supervisor. Note that it's started with no arguments. This is important as it defines main behaviour, and was necessary to allow for secondary GenServers used in testing.

Some networking code was added in `lib/remote_backend_exercise_web`:

- **controllers/points_controller.ex** - Defines simple access to the required users, by directly rendering the necessary JSON.
- **controllers/points_view.ex** - Controls the rendering process initiated by the controller. In this case it's formatting the date.
- **router.ex** - I've defined the root endpoint here, simply pointing it to `PointsController`.

`priv/repo` contains some additional database-related code:

- **migrations/20220207193055_create_users_table.exs** - Defines the user table
- **seeds.exs** - Inserts the initial set of users. It's done with `insert_all` chunk-by-chunk so as to avoid hitting the Postgres limits. This wasn't great and with more time I would explore it further.

Finally, the `tests` directory follows the same structure, and tests can be found there for the aforementioned modules.

### Some detail on the PointsManager implementation

You might notice `PointsManager` has some odd implementation details.

First of all, its `start_link` accepts an optional name argument that defaults to `__MODULE__`. This was done for testability reasons. In `PointsManager` test, the process is started using a custom atom registry name, which makes it run in a secondary version of the `GenServer`. This was done because the recommended method of testing a `GenServer` is to start it manually using `start_supervised/1`.

Second, a config property was defined for the module, `static_max_number`. It allows me to force a constant `max_number` in order to carefully assert results in testing. `Mock` wasn't usable for this, as it's not capable of mocking internal calls, so I couldn't mock the result of `random_max_number`.

## Assumptions, compromises, and potential next steps

- Timestamps were assumed to be UTC
- It was understood that "Queries the database for all users with more points than max_number but only retrieve a max of 2 users" means that the actual retrieval from the database was of a max of 2 users, not that the retrieval would include everything only to be truncated at the GenServer module
- Requirements didn't mention if these 2 users should be random, and I didn't assume so, they are simply the earliest ones. I considered ordering them by their point count, as the example seemed to do so, but I chose not to do it as it's not explicit in the requirements.
- Precision of timestamps is lowered for large-scale insertion in seeds
- The implementation of PointsManager isn't as clean as I wish due to constraints related to testing. With more time I would find a more elegant testing solution.
- Testing in PointsManager does not go over the every-minute changes, but the feature itself is tested in the `Users` module unit tests
