defmodule RemoteBackendExerciseWeb.PointsViewTest do
  use ExUnit.Case
  import RemoteBackendExerciseWeb.PointsView

  describe "render/2" do
    test "if timestamp is nil, return result as-is" do
      assert %{users: [%{id: 1, points: 1}], timestamp: nil} ==
               render("index.json", %{result: %{users: [%{id: 1, points: 1}], timestamp: nil}})
    end

    test "if timestamp is valid, return properly formatted" do
      datetime = ~U[2000-01-01 01:01:01.487685Z]

      assert %{users: [%{id: 1, points: 1}], timestamp: "2000-01-01 01:01:01"} ==
               render("index.json", %{
                 result: %{users: [%{id: 1, points: 1}], timestamp: datetime}
               })
    end
  end
end
