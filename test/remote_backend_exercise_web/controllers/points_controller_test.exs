defmodule RemoteBackendExerciseWeb.PointsControllerTest do
  use RemoteBackendExerciseWeb.ConnCase
  alias RemoteBackendExercise.Users

  @static_max_number Application.compile_env(:remote_backend_exercise, [
                       :points_manager,
                       :static_max_number
                     ])

  test "gets the correct information in the right format", %{conn: conn} do
    # This is asserted to cover for cases where the @static_max_number config value is out of bounds
    assert {:ok, _} = Users.add(%{points: @static_max_number - 1})
    assert {:ok, %{id: id1, points: points1}} = Users.add(%{points: @static_max_number + 2})
    assert {:ok, %{id: id2, points: points2}} = Users.add(%{points: @static_max_number + 3})
    assert {:ok, _} = Users.add(%{points: @static_max_number + 4})

    conn = get(conn, "/")

    assert %{
             "timestamp" => _,
             "users" => [
               %{"id" => ^id1, "points" => ^points1},
               %{"id" => ^id2, "points" => ^points2}
             ]
           } = json_response(conn, 200)
  end
end
