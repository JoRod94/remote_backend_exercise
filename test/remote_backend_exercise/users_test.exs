defmodule RemoteBackendExercise.UsersTest do
  use RemoteBackendExercise.DataCase, async: true
  alias RemoteBackendExercise.Users
  alias RemoteBackendExercise.Repo
  alias RemoteBackendExercise.Schemas.User

  describe "add/1" do
    test "properly adds a user with the given valid properties" do
      assert {:ok, %User{points: 23, id: id}} = Users.add(%{points: 23})

      assert %User{points: 23, id: ^id} = Repo.get(User, id)
    end

    test "fails with a changeset error when trying to add a user with >100 points" do
      assert {:error,
              %Ecto.Changeset{
                errors: [
                  points: {_, [validation: :number, kind: :less_than_or_equal_to, number: 100]}
                ]
              }} = Users.add(%{points: 101})
    end

    test "fails with a changeset error when trying to add a user with <0 points" do
      assert {:error,
              %Ecto.Changeset{
                errors: [
                  points: {_, [validation: :number, kind: :greater_than_or_equal_to, number: 0]}
                ]
              }} = Users.add(%{points: -1})
    end
  end

  describe "find/1" do
    test "properly retrieves the user of the given ID" do
      {:ok, %User{id: id}} = Users.add()

      assert {:ok, %User{id: ^id}} = Users.find(id)
    end

    test "returns :not_found error when missing" do
      assert {:error, :not_found} = Users.find(1234)
    end
  end

  describe "find_all_with_points_higher_than/1" do
    test "returns empty list if no users match the condition" do
      {:ok, _} = Users.add(%{points: 20})

      assert [] == Users.find_all_with_points_higher_than(23)
    end

    test "returns only one user if only one matches the condition" do
      {:ok, _} = Users.add(%{points: 20})
      {:ok, expected_user} = Users.add(%{points: 25})

      assert [expected_user] == Users.find_all_with_points_higher_than(23)
    end

    test "returns the two earliest valid users if more than two match the condition" do
      {:ok, _} = Users.add(%{points: 20})
      {:ok, expected_user1} = Users.add(%{points: 25})
      {:ok, expected_user2} = Users.add(%{points: 26})
      {:ok, _} = Users.add(%{points: 27})

      assert [expected_user1, expected_user2] == Users.find_all_with_points_higher_than(23)
    end
  end

  describe "update/2" do
    test "properly updates a user with the given valid properties" do
      {:ok, %User{points: 1, id: id}} = Users.add(%{points: 1})
      assert {:ok, %User{points: 3, id: id}} = Users.update(id, %{points: 3})

      assert %User{points: 3, id: ^id} = Repo.get(User, id)
    end

    test "fails with a changeset error when trying to update a user to >100 points" do
      {:ok, %User{points: 1, id: id}} = Users.add(%{points: 1})
      assert {:ok, %User{points: 3, id: id}} = Users.update(id, %{points: 3})

      assert {:error,
              %Ecto.Changeset{
                errors: [
                  points: {_, [validation: :number, kind: :less_than_or_equal_to, number: 100]}
                ]
              }} = Users.update(id, %{points: 101})
    end

    test "fails with a changeset error when trying to update a user to <0 points" do
      {:ok, %User{points: 1, id: id}} = Users.add(%{points: 1})
      assert {:ok, %User{points: 3, id: id}} = Users.update(id, %{points: 3})

      assert {:error,
              %Ecto.Changeset{
                errors: [
                  points: {_, [validation: :number, kind: :greater_than_or_equal_to, number: 0]}
                ]
              }} = Users.update(id, %{points: -1})
    end
  end

  describe "update_all_to_random_points/0" do
    test "all users' points are updated to random values" do
      before_time =
        DateTime.utc_now()
        |> DateTime.add(-60 * 60, :second)
        |> DateTime.to_naive()
        |> NaiveDateTime.truncate(:second)

      %User{id: 1, points: 1, inserted_at: before_time, updated_at: before_time}
      |> Repo.insert!()

      %User{id: 2, points: 2, inserted_at: before_time, updated_at: before_time}
      |> Repo.insert!()

      Users.update_all_to_random_points()

      assert {:ok, user1} = Users.find(1)
      assert user1.points != 1
      assert NaiveDateTime.compare(user1.updated_at, before_time) == :gt

      assert {:ok, user2} = Users.find(2)
      assert user2.points != 2
      assert NaiveDateTime.compare(user2.updated_at, before_time) == :gt
    end
  end
end
