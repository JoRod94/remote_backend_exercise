defmodule RemoteBackendExercise.PointsManagerTest do
  use RemoteBackendExercise.DataCase
  import Mock
  alias RemoteBackendExercise.PointsManager
  alias RemoteBackendExercise.Users

  @test_points_manager_registry_name :test_points_manager

  @static_max_number Application.compile_env(:remote_backend_exercise, [
                       :points_manager,
                       :static_max_number
                     ])

  # Note that a static max number has been defined in config/test.exs
  describe "get_users" do
    test "returns empty list if no users match the condition, and nil timestamp on first call" do
      start_test_points_manager()

      # This is asserted to cover for cases where the @static_max_number config value is out of bounds
      assert {:ok, _} = Users.add(%{points: @static_max_number - 1})
      assert %{users: [], timestamp: nil} = get_users_from_test_points_manager()
    end

    test "returns only one user if only one matches the condition, and the previous timestamp" do
      start_test_points_manager()

      # This is asserted to cover for cases where the @static_max_number config value is out of bounds
      assert {:ok, _} = Users.add(%{points: @static_max_number - 1})
      assert {:ok, expected_user} = Users.add(%{points: @static_max_number + 1})

      datetime = ~U[2000-01-01 01:01:01.000000Z]

      with_mock DateTime, [], utc_now: fn -> datetime end do
        assert %{users: [expected_user], timestamp: nil} == get_users_from_test_points_manager()

        assert %{users: [expected_user], timestamp: datetime} ==
                 get_users_from_test_points_manager()
      end
    end

    test "returns the two earliest valid users if more than two match the condition" do
      start_test_points_manager()

      # This is asserted to cover for cases where the @static_max_number config value is out of bounds
      assert {:ok, _} = Users.add(%{points: @static_max_number - 1})
      assert {:ok, expected_user1} = Users.add(%{points: @static_max_number + 2})
      assert {:ok, expected_user2} = Users.add(%{points: @static_max_number + 3})
      assert {:ok, _} = Users.add(%{points: @static_max_number + 4})

      datetime = ~U[2000-01-01 01:01:01.000000Z]

      with_mock DateTime, [], utc_now: fn -> datetime end do
        assert %{
                 users: [expected_user1, expected_user2],
                 timestamp: nil
               } == get_users_from_test_points_manager()

        assert %{users: [expected_user1, expected_user2], timestamp: datetime} ==
                 get_users_from_test_points_manager()
      end
    end
  end

  defp start_test_points_manager do
    start_supervised!({PointsManager, @test_points_manager_registry_name})
  end

  defp get_users_from_test_points_manager,
    do: PointsManager.get_users(@test_points_manager_registry_name)
end
