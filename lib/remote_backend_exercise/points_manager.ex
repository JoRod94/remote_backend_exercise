defmodule RemoteBackendExercise.PointsManager do
  @moduledoc """
  PointsManager is responsible for user points

  It manages a max_number state variable that is updated
    to a random value every minute

  It keeps a timestamp state variable that indicates 
    the time of the last query

  State is therefore: {max_number, timestamp}

  It handles a call that returns a max of 2 users with more points than
    the current max_number
  """

  use GenServer
  alias RemoteBackendExercise.Users

  @static_max_number Application.compile_env(:remote_backend_exercise, [
                       :points_manager,
                       :static_max_number
                     ])

  @doc """
    Starts the GenServer, this should be called by a Supervisor
  """
  @spec start_link(module()) :: GenServer.on_start()

  def start_link(name \\ __MODULE__) do
    GenServer.start_link(__MODULE__, {random_max_number(), nil}, name: name)
  end

  @type get_users_response :: %{
          users: list(Ecto.Schema.t()),
          timestamp: DateTime.t()
        }

  @doc """
    Gets users that have a point count higher than the current max_number

    Returns a map including these users and the timestamp of the latest query
  """
  @spec get_users(module()) :: get_users_response()
  def get_users(name \\ __MODULE__) do
    GenServer.call(name, :get_users)
  end

  @impl true
  def init(state) do
    schedule_number_update()
    {:ok, state}
  end

  @impl true
  def handle_info(:update_number, {_max_number, timestamp}) do
    schedule_number_update()

    Users.update_all_to_random_points()

    {:noreply, {random_max_number(), timestamp}}
  end

  @impl true
  def handle_call(:get_users, _from, {max_number, current_timestamp}) do
    users = Users.find_all_with_points_higher_than(max_number)
    new_timestamp = DateTime.utc_now()

    result = %{users: users, timestamp: current_timestamp}

    new_state = {max_number, new_timestamp}

    {:reply, result, new_state}
  end

  @spec random_max_number() :: integer()
  defp random_max_number do
    case @static_max_number do
      nil ->
        :rand.uniform(101) - 1

      static_max_number ->
        static_max_number
    end
  end

  defp schedule_number_update do
    Process.send_after(self(), :update_number, 60 * 1000)
  end
end
