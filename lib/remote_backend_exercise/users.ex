defmodule RemoteBackendExercise.Users do
  @moduledoc """
  Defines functions to manipulate records of our User model
  """

  import Ecto.Query, only: [from: 2]
  alias RemoteBackendExercise.Repo
  alias RemoteBackendExercise.Schemas.User

  @typedoc """
    User properties to be added or changed
  """
  @type params :: map()

  @type id :: number()

  @doc """
    Adds a new user with the given params
    
    Will return a tuple with either the created user or a changeset error
  """
  @spec add(params) :: {:error, Ecto.Changeset.t()} | {:ok, Ecto.Schema.t()}
  def add(params \\ %{}) do
    %User{points: 0}
    |> User.changeset(params)
    |> Repo.insert()
  end

  @doc """
    Finds the user with the given ID
    
    Will return a tuple with either the found user or a :not_found error
  """
  @spec find(id) :: {:error, :not_found} | {:ok, Ecto.Schema.t()}
  def find(id) do
    case User
         |> Repo.get(id) do
      nil ->
        {:error, :not_found}

      result ->
        {:ok, result}
    end
  end

  @doc """
    Finds all users with a point count higher than the argument
    
    Will return the list of users that fit this criterion
  """
  @spec find_all_with_points_higher_than(number()) :: list({:ok, Ecto.Schema.t()})
  def find_all_with_points_higher_than(points) do
    query =
      from u in User,
        where: u.points > ^points,
        limit: 2

    Repo.all(query)
  end

  @doc """
    Updates the user with the given ID
    
    Will return a tuple with either the updated user or a changeset error
  """
  @spec update(id, params) :: {:error, Ecto.Changeset.t()} | {:ok, Ecto.Schema.t()}
  def update(id, params) do
    with {:ok, user} <- find(id) do
      user
      |> User.changeset(params)
      |> Repo.update()
    end
  end

  @doc """
    Updates all users to random points amounts
    
    Will return a tuple with either the updated user or a changeset error
  """
  @spec update_all_to_random_points() :: {integer(), nil | [term()]}
  def update_all_to_random_points do
    Ecto.Query.update(User,
      set: [
        points: fragment("floor(random()*100)"),
        updated_at: fragment("now() at time zone 'utc'")
      ]
    )
    |> Repo.update_all([])
  end
end
