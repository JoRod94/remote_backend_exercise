defmodule RemoteBackendExercise.Schemas.User do
  @moduledoc """
  Defines the User model in our context, an entity with a given amount of points
  """
  use Ecto.Schema
  import Ecto.Changeset

  @attrs [
    :points
  ]

  # the only properties we want to expose are these
  @derive {Jason.Encoder, only: [:id, :points]}
  schema "users" do
    field :points, :integer

    timestamps()
  end

  def changeset(user, attrs \\ %{}) do
    user
    |> cast(attrs, @attrs)
    |> validate_required(@attrs)
    |> validate_number(:points, greater_than_or_equal_to: 0, less_than_or_equal_to: 100)
  end
end
