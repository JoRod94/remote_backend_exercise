defmodule RemoteBackendExerciseWeb.PointsController do
  @moduledoc """
  Controller responsible for handling API calls for user points
  """
  use RemoteBackendExerciseWeb, :controller
  alias RemoteBackendExercise.PointsManager

  def index(conn, _params) do
    render(conn, "index.json", result: PointsManager.get_users())
  end
end
