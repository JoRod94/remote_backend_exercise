defmodule RemoteBackendExerciseWeb.PointsView do
  @moduledoc """
  The view of the points controller
  Responsible for defining the rendered output
  """
  use RemoteBackendExerciseWeb, :view

  def render("index.json", %{result: %{timestamp: nil} = result}), do: result

  def render("index.json", %{result: %{users: users, timestamp: timestamp}}) do
    %{
      users: users,
      timestamp:
        timestamp
        |> DateTime.truncate(:second)
        |> DateTime.to_naive()
        |> NaiveDateTime.to_string()
    }
  end
end
