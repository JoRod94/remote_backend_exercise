defmodule RemoteBackendExerciseWeb.Router do
  use RemoteBackendExerciseWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {RemoteBackendExerciseWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", RemoteBackendExerciseWeb do
    get "/", PointsController, :index
  end
end
