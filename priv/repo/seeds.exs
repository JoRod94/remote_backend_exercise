# 20_000 is a reasonable approximation given the postgres limit for insert_all
0..1_000_000
|> Enum.map(fn _ ->
  [
    points: 0,
    inserted_at: DateTime.utc_now() |> DateTime.to_naive() |> NaiveDateTime.truncate(:second),
    updated_at: DateTime.utc_now() |> DateTime.to_naive() |> NaiveDateTime.truncate(:second)
  ]
end)
|> Enum.chunk_every(20_000)
|> Enum.each(fn chunk ->
  RemoteBackendExercise.Repo.insert_all(
    RemoteBackendExercise.Schemas.User,
    chunk
  )
end)
